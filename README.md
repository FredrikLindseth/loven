# Norges lover

## Lover

Mappe med lover. Alle lovene har undermappene `endringer` som inneholder alle endringsforskriftene, og `historiske` som inneholder alle historiske versjoner av lovene.

### [Kongeriket Norges Grunnlov](./norges_lover/Lover/kongeriket_norges_grunnlov/kongeriket_norges_grunnlov.md)

[lovdata](https://lovdata.no/pro/#document/NL/lov/1814-05-17)

## Forskrifter

Mappe med forkrifter. Alle forskritene har undermappene `endringer` som inneholder alle endringsforskriftene, og `historiske` som inneholder alle historiske versjoner av forskriftene.

### [Forskrift om birøkt](./norges_lover/Forskrifter/om_birøkt/forskrift_om_birøkt.md)

[lovdata](https://lovdata.no/pro/#document/SF/forskrift/2009-04-06-416)

### [Forskrift om import av brukt bimateriell](./norges_lover/Forskrifter/import_av_brukt_bimateriell/forskrift_om_import_av_brukt_bimateriell.md)

[lovdata](https://lovdata.no/pro/#document/SFO/forskrift/2008-04-30-417)

### [Forskrift om import og eksport av biavlsprodukter og brukt bimateriell](./norges_lover/Forskrifter/import_og_eksport_av_biavlsprodukter_og_brukt_bimateriell/forskrift_om_import_og_eksport_av_biavlsprodukter_og_brukt_bimateriell.md)

[lovdata](https://lovdata.no/pro/#document/SFO/forskrift/2001-02-21-170)

### [Forskrift om tiltak mot sjukdommer på bier](./norges_lover/Forskrifter/tiltak_mot_sjukdommer_på_bier/forskrift_om_tiltak_mot_sjukdommer_på_bier.md)

[lovdata](https://lovdata.no/pro/#document/SFO/forskrift/2001-04-27-437)

### [Forskrift om innførsel og utførsel av levende bier, bisæd, biembryoner, biavlsprodukter og brukt bimateriell](./norges_lover/Forskrifter/innførsel_og_utførsel_av_levende_bier/forskrift_om_innførsel_og_utførsel_av_levende_bier.md)

[lovdata](https://lovdata.no/pro/#document/SFO/forskrift/1999-05-03-636)

## Om

Inspirasjon

- [GitLaw: GitHub For Laws And Legal Documents - A Tourniquet For American Liberty](http://blog.abevoelker.com/gitlaw-github-for-laws-and-legal-documents-a-tourniquet-for-american-liberty)
- [Ycombinator om saken over](https://news.ycombinator.com/item?id=3967921)
- [Ars Technica: How I changed the law with a GitHub pull request](https://arstechnica.com/tech-policy/2018/11/how-i-changed-the-law-with-a-github-pull-request)
- [Eksempel github repo](https://github.com/DCCouncil/dc-law-xml)

## Todo

- Få inn alle endringsforskriftene til grunnloven
- Oppdater alle .md-forskrift og lov-filene. Star med orginalloven og oppdater fram over med en ordentlig commitmelding
- Fiks resten av lovene