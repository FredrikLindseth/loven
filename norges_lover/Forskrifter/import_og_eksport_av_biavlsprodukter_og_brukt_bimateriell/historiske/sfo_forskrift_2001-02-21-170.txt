﻿Utskrift fra Lovdata - 04.04.2019 21:12

 

Forskrift om import og eksport av biavlsprodukter og brukt bimateriell.

OPPHEVET

Dato
FOR-2001-02-21-170
Departement
Landbruks- og matdepartementet
Avd/dir
Avd. for matpolitikk
Publisert
I 2001 hefte 2
Ikrafttredelse
21.02.2001
Sist endret
FOR-2008-04-30-417
Endrer
FOR-1999-05-03-636
Gjelder for
Norge
Hjemmel
LOV-2003-12-19-124-§5, LOV-2003-12-19-124-§7, LOV-2003-12-19-124-§12, LOV-2003-12-19-124-§15, LOV-2003-12-19-124-§19, LOV-2003-12-19-124-§33, FOR-2003-12-19-1790
Kunngjort
06.03.2001
Korttittel
Forskrift om import og eksport av bier mv.



Innholdsfortegnelse
Forskrift om import og eksport av biavlsprodukter og brukt bimateriell.	1
Innholdsfortegnelse	2
Kap. I. Formål, virkeområde og definisjoner	3
§ 1. Formål og virkeområde	3
§ 2. Definisjoner	3
Kap. II. Import og eksport av biavlsprodukter innenfor EØS	3
§ 3. Registrering av eksportør	3
§ 4. (Opphevet ved forskrift 20 feb 2004 nr. 483.)	3
§ 5. Vilkår for import og eksport	3
§ 6. (Opphevet ved forskrift 20 feb 2004 nr. 483.)	4
§ 7. Merking	4
§ 8. Dokumentasjon	4
Kap. III. Forbud	4
§ 9. Forbud	4
Kap. IV. Avsluttende bestemmelser	4
§ 10. Tilsyn og kontroll	4
§ 11. Utgifter	4
§ 12. Dispensasjon	5
§ 13. Sikkerhetsbestemmelser	5
§ 14. Straff	5
§ 15. Ikrafttreden	5


Hjemmel: Fastsatt av Landbruksdepartementet 21. februar 2001 med hjemmel i lov av 19. desember 2003 nr. 124 om matproduksjon og mattrygghet mv. (matloven) § 5, § 7, § 12, § 15, § 19, § 33 første ledd, jf. § 36 andre ledd, jf. delegeringsvedtak av 19. desember 2003 nr. 1790.
EØS-henvisninger: EØS-avtalen vedlegg I (direktiv 92/65/EØF og direktiv 92/118/EØF).
Endringer: Endret ved forskrifter 9 jan 2004 nr. 89 (bl.a hjemmel), 20 feb 2004 nr. 483 (bl.a tittel).
Opphevet 30 april 2008, jf. forskrift 30 april 2008 nr. 417.


Kap. I. Formål, virkeområde og definisjoner

§ 1. Formål og virkeområde
Formålet med denne forskrift er å forebygge spredning av smittsomme sjukdommer hos bier ved å fastsette:
1.
vilkår for import og eksport av biavlsprodukter innenfor EØS,

2.
forbud mot import av biavlsprodukter fra land utenfor EØS,

3.
forbud mot import av brukt bimateriell.


0
Endret ved forskrift 20 feb 2004 nr. 483.


§ 2. Definisjoner
I denne forskrift menes med:

Biavlsprodukter: Honning, bivoks, dronninggéle, proppolis og pollen til bruk i birøkt.

Brukt bimateriell: Brukte bikuber og annet utstyr som har vært brukt i birøkt.

Eksportør: Juridisk eller fysisk person som eier eller har ansvaret for bedrift hvorfra biavlsprodukter eksporteres.

Bedrift: Anlegg, herunder bigård, hvor biavlsprodukter produseres, lagres eller behandles.

0
Endret ved forskrifter 9 jan 2004 nr. 89, 20 feb 2004 nr. 483.



Kap. II. Import og eksport av biavlsprodukter innenfor EØS

0
Overskriften endret ved forskrift 20 feb 2004 nr. 483.


§ 3. Registrering av eksportør
Eksportør av biavlsprodukter til land i EØS skal være registrert av Mattilsynet. Søknad om registrering skal skje skriftlig og følges av en erklæring der eksportøren forplikter seg til å:
1.
la bedriften være gjenstand for inspeksjon av Mattilsynet,

2.
bare eksportere biavlsprodukter som oppfyller vilkårene i § 5, og

3.
sørge for at forsendelsene merkes i henhold til § 7 samt følges av dokumentasjon i henhold til § 8.

Registreringen gjelder for 3 år, men Mattilsynet kan trekke den tilbake tidligere dersom eksportøren ikke etterlever forpliktelsene etter § 3 første ledd.

0
Endret ved forskrifter 9 jan 2004 nr. 89, 20 feb 2004 nr. 483.


§ 4. (Opphevet ved forskrift 20 feb 2004 nr. 483.)

§ 5. Vilkår for import og eksport
Biavlsprodukter som importeres fra eller eksporteres til land i EØS skal:
1.
komme fra eksportør som er registrert av kompetent myndighet i avsenderlandet,

2.
ikke komme fra område eller bedrift hvor det er restriksjoner på grunn av smittsom bisjukdom,

3.
komme fra et område hvor det ikke er restriksjoner som følge av lukket yngelråte, og

4.
oppfylle eventuelle tilleggsgarantier som mottakerlandet er innvilget.


0
Endret ved forskrift 20 feb 2004 nr. 483.


§ 6. (Opphevet ved forskrift 20 feb 2004 nr. 483.)

§ 7. Merking
Enhver forsendelse av biavlsprodukter skal være merket på en slik måte at det er mulig å fastslå hvilket land og hvilken bigård eller bedrift den kommer fra.

0
Endret ved forskrift 20 feb 2004 nr. 483.


§ 8. Dokumentasjon
Med hver forsendelse av biavlsprodukter skal det følge et handelsdokument utstedt av eksportøren. Dokumentet skal angi produktets navn og art og bedriftens registreringsnummer, samt inneholde en bekreftelse fra eksportøren om at vilkårene i § 5 er oppfylt. Originaldokumentet skal følge forsendelsen fram til mottaker.

0
Endret ved forskrift 20 feb 2004 nr. 483.



Kap. III. Forbud

§ 9. Forbud
Det er forbudt å importere:
1.
biavlsprodukter fra land utenfor EØS,

2.
brukt bimateriell, unntatt utstyr til honningbehandling som er rengjort og desinfisert.


0
Endret ved forskrift 20 feb 2004 nr. 483.



Kap. IV. Avsluttende bestemmelser

§ 10. Tilsyn og kontroll
Ved import av biavlsprodukter kommer gjeldende forskrifter om tilsyn og kontroll til anvendelse.1

0
Endret ved forskrift 20 feb 2004 nr. 483.

1
Jf. forskrift 23. desember 1998 nr. 1471 om tilsyn og kontroll ved import og eksport av næringsmidler og av produkter av animalsk opprinnelse innen EØS, og av ikke-animalske næringsmidler fra tredjeland, forskrift 18. oktober 1999 nr. 1163 om tilsyn og kontroll ved import og transitt mv., animalske næringsmidler og produkter av animalsk opprinnelse mv. fra tredjeland og forskrift 20. januar 2000 nr. 47 om gebyr for tilsyn og kontroll ved import og transitt mv. av levende dyr, animalske næringsmidler og produkter av animalsk opprinnelse mv. fra tredjeland.


§ 11. Utgifter
Alle utgifter forbundet med import og eksport av biavlsprodukter og brukt bimateriell er det offentlige uvedkommende.

0
Endret ved forskrift 20 feb 2004 nr. 483.


§ 12. Dispensasjon
Mattilsynet kan dispensere fra bestemmelsene i denne forskriften og sette vilkår for dispensasjonen, forutsatt at det ikke vil stride mot Norges internasjonale forpliktelser, herunder EØS-avtalen.

0
Endret ved forskrifter 9 jan 2004 nr. 89, 20 feb 2004 nr. 483.


§ 13. Sikkerhetsbestemmelser
Mattilsynet kan på kort varsel, og uten erstatning for importør eller eksportør, stoppe import og eksport av biavlsprodukter og brukt bimateriell dersom spesielle forhold vedrørende avsenderlandets dyrehelsesituasjon eller andre forhold gjør dette nødvendig.

0
Endret ved forskrift 20 feb 2004 nr. 483.


§ 14. Straff
Forsettlig eller uaktsom overtredelse av denne forskrift er straffbar, jf. matloven § 28.

0
Endret ved forskrifter 9 jan 2004 nr. 89, 20 feb 2004 nr. 483.


§ 15. Ikrafttreden
Denne forskrift trer i kraft straks. Fra samme tidspunkt oppheves forskrift av 3. mai 1999 nr. 636 om innførsel og utførsel av levende bier, bisæd, biembryoner, biavlsprodukter og bimateriell.
 
Forskrift om import og eksport av biavlsprodukter og brukt bimateriell.
Side 5
