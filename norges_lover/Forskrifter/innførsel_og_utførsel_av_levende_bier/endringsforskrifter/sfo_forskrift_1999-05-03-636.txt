﻿Utskrift fra Lovdata - 04.04.2019 21:58

 

Forskrift om innførsel og utførsel av levende bier, bisæd, biembryoner, biavlsprodukter og brukt bimateriell.

OPPHEVET

Dato
FOR-1999-05-03-636
Departement
Landbruks- og matdepartementet
Avd/dir
Avd. for matproduksjon og helse
Publisert
I 1999 1431
Ikrafttredelse
03.05.1999
Sist endret
FOR-2001-02-21-170
Gjelder for
Norge
Hjemmel
LOV-1938-06-03-3-§8
Korttittel
Forskrift om handel med levende bier mv



Innholdsfortegnelse
Forskrift om innførsel og utførsel av levende bier, bisæd, biembryoner, biavlsprodukter og brukt bimateriell.	1
Innholdsfortegnelse	2
Kap. I Formål, virkeområde og definisjoner	3
§ 1. Formål og virkeområde	3
§ 2. Definisjoner	3
Kap. II Vilkår for import fra/eksport til land i EØS av levende bier, bisæd, biembryoner og biavlsprodukter	3
§ 3. Registrering av eksportør	3
§ 4. Levende bier	4
§ 5. Biavlsprodukter	4
§ 6. Bisæd og biembryoner	4
§ 7. Merking	4
§ 8. Dokumentasjon	4
Kap. III Forbud	4
§ 9. Forbud	4
Kap. IV Avsluttende bestemmelser	5
§ 10. Tilsyn og kontroll	5
§ 11. Utgifter	5
§ 12. Dispensasjon	5
§ 13. Sikkerhetsbestemmelser	5
§ 14. Straffebestemmelser	5
§ 15. Ikrafttreden	5


Fastsatt av Landbruksdepartementet 3. mai 1999 med hjemmel i lov av 3. juni 1938 nr. 3 om smittsomme sykdommer på bier § 8, jf. EØS-avtalens vedlegg I, Rdir. 92/65/EØF og Rdir. 92/118/EØF. Opphevet fra 21 feb 2001, jf forskrift 21 feb 2001 nr. 170.


Kap. I Formål, virkeområde og definisjoner

§ 1. Formål og virkeområde
Formålet med denne forskriften er å forebygge spredning av smittsomme sjukdommer på bier i forbindelse med innførsel og utførsel ved å fastsette:
1.
vilkår for import fra og eksport til land i EØS av levende bier og biavlsprodukter,

2.
vilkår for import av bisæd og biembryoner fra land i EØS,

3.
forbud mot annen innførsel enn det som er definert som import av levende bier, bisæd, biembryoner og biavlsprodukter fra land i EØS, herunder å drive vandrebirøkt over riksgrensene,

4.
forbud mot innførsel av levende bier, bisæd, biembryoner og biavlsprodukter fra land utenfor EØS,

5.
forbud mot innførsel av brukt bimateriell, unntatt utstyr til honningbehandling som er rengjort og desinfisert.


§ 2. Definisjoner
I denne forskriften menes med:
Bier: Honningbier (Apis mellifera).
Biavlsprodukter: Honning, bivoks, dronninggele, propolis og pollen til bruk i birøkt.
Brukt bimateriell: Brukte bikuber og annet utstyr som har vært brukt i birøkt.
Innførsel/utførsel: Grensepassering.
Import/eksport: Grensepassering i forbindelse med overdragelse av eiendomsrett.
Eksportør: Juridisk eller fysisk person som eier eller har ansvaret for bigård/bedrift hvorfra levende bier og/eller biavlsprodukter eksporteres til land i EØS.
Vandrebirøkt: Midlertidig flytting av bikuber for pollinering eller utnytting av trekk.
Bigård: Virksomhet hvor bier holdes og/eller omsettes.
Bedrift: Anlegg, herunder bigård, hvor biavlsprodukter produseres, lagres og/eller behandles.
Offentlig veterinær: Veterinær gitt offentlig myndighet i det aktuelle land. I Norge; Statens dyrehelsetilsyn – distriktsveterinæren.


Kap. II Vilkår for import fra/eksport til land i EØS av levende bier, bisæd, biembryoner og biavlsprodukter

§ 3. Registrering av eksportør
Eksportør av levende bier og/eller biavlsprodukter til land i EØS skal være registrert av Statens dyrehelsetilsyn – fylkesveterinæren. Søknad om registrering skal skje skriftlig og følges av en erklæring der eksportøren forplikter seg til å:
1.
la bigården/bedriften være gjenstand for inspeksjon av Statens dyrehelsetilsyn – distriktsveterinæren,

2.
bare eksportere bier og/eller biavlsprodukter som oppfyller kravene i henholdsvis § 4 og § 5 og sørge for at forsendelsene merkes i henhold til § 7 og følges av dokumentasjon i henhold til § 8. Registreringen gjelder for 3 år, men fylkesveterinæren kan trekke den tilbake tidligere dersom eksportøren ikke etterlever forpliktelsene etter første ledd.


§ 4. Levende bier
Levende bier, inkludert larver og pupper, som importeres fra eller eksporteres til land i EØS skal:
1.
komme fra eksportør som er registrert av kompetent myndighet i avsenderlandet,

2.
ikke vise tegn på sjukdom eller komme fra område eller bigård hvor det er restriksjoner på grunn av smittsom bisjukdom,

3.
komme fra et område hvor det ikke er restriksjoner som følge av lukket yngelråte, og

4.
oppfylle eventuelle tilleggsgarantier som mottakerlandet er innvilget.


§ 5. Biavlsprodukter
Biavlsprodukter som importeres fra eller eksporteres til land i EØS skal:
1.
komme fra eksportør som er registrert av kompetent myndighet i avsenderlandet,

2.
ikke komme fra område eller bedrift hvor det er restriksjoner på grunn av smittsom bisjukdom,

3.
komme fra et område hvor det ikke er restriksjoner som følge av lukket yngelråte, og

4.
oppfylle eventuelle tilleggsgarantier som mottakerlandet er innvilget.


§ 6. Bisæd og biembryoner
Bisæd og biembryoner som importeres fra land i EØS skal:
1.
komme fra eksportør som er registrert av kompetent myndighet i avsenderlandet,

2.
stamme fra bier som ikke viser tegn til sjukdom eller kommer fra område eller bigård hvor det er restriksjoner på grunn av smittsom bisjukdom, og

3.
komme fra et område hvor det ikke er restriksjoner som følge av lukket yngelråte.


§ 7. Merking
Enhver forsendelse av levende bier, bisæd, biembryoner og biavlsprodukter skal være merket på en slik måte at det er mulig å fastslå hvilket land og hvilken bigård eller bedrift den kommer fra.

§ 8. Dokumentasjon
Med hver forsendelse av levende bier, bisæd eller biembryoner skal det følge et helsesertifikat utstedt av offentlig veterinær i avsenderlandet i løpet av det siste døgnet før avsendelse. Sertifikatet skal inneholde en bekreftelse om at forsendelsen oppfyller vilkårene i henholdsvis § 4 og § 6. Originalsertifikatet, som er gyldig i 10 dager etter utstedelse, skal følge forsendelsen fram til mottaker.
Med hver forsendelse av biavlsprodukter skal det følge et handelsdokument utferdiget av eksportøren. Av dokumentet skal det framgå produktets navn og art, bedriftens registreringsnummer og at forsendelsen oppfyller vilkårene i § 5. Originaldokumentet skal følge forsendelsen fram til mottaker.


Kap. III Forbud

§ 9. Forbud
Det er forbudt å innføre:
1.
levende bier, bisæd, biembryoner og biavlsprodukter fra land i EØS når innførselen ikke er definert som import jf. § 2, herunder å drive vandrebirøkt over riksgrensene,

2.
levende bier, bisæd, biembryoner og biavlsprodukter fra land utenfor EØS,

3.
brukt bimateriell med unntak av utstyr til honningbehandling som er rengjort og desinfisert.



Kap. IV Avsluttende bestemmelser

§ 10. Tilsyn og kontroll
Tilsyn og kontroll med innførsel og utførsel av levende bier, bisæd, biembryoner og biavlsprodukter skal føres i henhold til bestemmelsene i forskrift 31. desember 1998 nr. 1484 om tilsyn og kontroll ved innførsel og utførsel av levende dyr, annet avlsmateriale og animalsk avfall og forskrift 23. desember 1998 nr. 1471 om tilsyn og kontroll ved import og eksport av næringsmidler og av produkter av animalsk opprinnelse.

§ 11. Utgifter
Alle utgifter forbundet med innførsel og utførsel av levende bier, bisæd, biembryoner, biavlsprodukter og brukt bimateriell er det offentlige uvedkommende.

§ 12. Dispensasjon
Statens dyrehelsetilsyn – Sentralforvaltningen kan dispensere fra bestemmelsene i denne forskriften og sette vilkår for slik dispensasjon.

§ 13. Sikkerhetsbestemmelser
Landbruksdepartementet kan på kort varsel og uten erstatning for importør eller eksportør, stoppe innførsel eller utførsel av levende bier, bisæd, biembryoner, biavlsprodukter og brukt bimateriell dersom spesielle forhold vedrørende avsenderlandets dyrehelsesituasjon eller andre forhold skulle tilsi det.

§ 14. Straffebestemmelser
Overtredelse av bestemmelsene gitt i denne forskriften er straffbart, jf. lov om smittsomme sjukdommer på bier av 3. juni 1938 nr. 3, § 12.

§ 15. Ikrafttreden
Denne forskrift trer i kraft 3. mai 1999.
 
Forskrift om innførsel og utførsel av levende bier, bisæd, biembryoner, biavlsprodukter og brukt bimateriell.
Side 5
