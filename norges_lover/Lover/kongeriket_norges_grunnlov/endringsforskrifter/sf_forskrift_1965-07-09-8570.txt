﻿Utskrift fra Lovdata - 04.04.2019 23:09

 

Forskrift om fastsettelse av vilkår for statens grunnkjøpsobligasjoner


Dato
FOR-1965-07-09-8570
Departement
Finansdepartementet
Avd/dir
Økonomiavd.
Gjelder for
Norge
Hjemmel
Stortingsvedtak, LOV-1814-05-17-§75
Korttittel
Forskrift om vilkår, statens grunnkjøpsobl.



Innholdsfortegnelse
Forskrift om fastsettelse av vilkår for statens grunnkjøpsobligasjoner	1
Innholdsfortegnelse	2
I	3




I
Med hjemmel i Stortingets vedtak av 4. mai 1965 gis Finansdepartementet fullmakt til å utstede grunnkjøpsobligasjoner til oppgjør for grunnervervelser for et beløp av inntil 100 mill. kroner med følgende vilkår:
1.
Rentesats 4 3/4 pst. p.a. som beregnes fra det tidspunkt de enkelte obligasjoner blir effektive.

2.
Løpetid. Obligasjonsserie A: 20 år – tilbakebetaling med like store halvårlige avdrag. Obligasjonsserie B: 15 år – avdrag ved halvårlige annuiteter. Obligasjonsserie C: 10 år – avdragsfritt, innløsning i sin helhet etter 10 år.

3.
Obligasjonseierne kan til enhver tid oppsi obligasjonene til innløsning gjennom Norges Bank med 3 måneders varsel, dog slik at de ikke kan innløses tidligere enn 6 måneder etter utstedelsen.

Obligasjonene kan ikke omsettes eller pantsettes, men kan overføres ved arv.
4.
Norges Bank utsteder sertifikater til obligasjonseierne, lydende på et bestemt antall grunnkjøpsobligasjoner a kr. 100,-. Obligasjonene innskrives på navn i register i Norges Bank, som på statens vegne utbetaler renter og avdrag på obligasjonene.

 
Forskrift om fastsettelse av vilkår for statens grunnkjøpsobligasjoner
Side 3
