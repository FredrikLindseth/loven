﻿Utskrift fra Lovdata - 04.04.2019 23:12

 

Forskrift om retningslinjer for tildeling av lån fra Statens fond for alkoholfrie overnattings- og serveringssteder.


Dato
FOR-1987-06-05-477
Departement
Helse- og omsorgsdepartementet
Avd/dir
Folkehelseavd.
Publisert
I 1987 444
Ikrafttredelse
23.07.1987
Sist endret
FOR-2013-01-17-61
Gjelder for
Norge
Hjemmel
LOV-1814-05-17-§75, Stortingsvedtak
Korttittel
Fors. om lån fra fond for alkoholfrie steder



Innholdsfortegnelse
Forskrift om retningslinjer for tildeling av lån fra Statens fond for alkoholfrie overnattings- og serveringssteder.	1
Innholdsfortegnelse	2
§ 1. Formålet med fondet er å bidra til å fremme edruskap, særlig blant unge, ved å gi lån til overnattings- og serveringssteder der det ikke serveres alkoholholdige drikkevarer.	3
§ 2. Lån til overnattingssteder gis til restaurant- eller kafedrift. Lån til investeringer i forbindelse med overnattingsdriften gis bare unntaksvis når særlige grunner foreligger.	3
§ 3. I særlige tilfeller kan lån ytes til tegning av nødvendig aksjekapital, eventuelt til kjøp av aksjer i bedrifter som nevnt under § 1.	3
§ 4. Fondets midler kan ikke nyttes til tilskudd.	3
§ 5. Lån skal sikres ved pant i fast eiendom, – herunder festerett til fast eiendom, – med driftstilbehør. Låntakeren skal holde pantet tilstrekkelig forsikret.	3
§ 6. Lån skal først utbetales når bygget er ferdig i samsvar med godkjente planer.	3
§ 7. Avdragene fastsettes etter annuitetsprinsippet med en fast minstesum (renter og avdrag) årlig. Rentesatsen skal være den vanlige for lån fra statens fond. Lånetiden skal være maksimum 20 år.	3
§ 8. Bedriften må ikke selges eller nyttes til annet formål uten samtykke fra Helsedirektoratet.	3
§ 9. Bedriftens regnskaper skal revideres av en registrert revisor i henhold til revisorloven. Denne kan gi instruksjoner om regnskapsføringen.	3
§ 10. Så lenge lånet ikke er tilbakebetalt, skal direktoratet føre slik regnskapsmessig og annen kontroll som det til enhver tid finner nødvendig, og lånetakerne er pliktig til å sende inn alle regnskapsoppgaver m.v. og opplysninger som måtte ønskes til dette formål.	3
§ 11. Statens nærings- og distriktutviklingsfond er regnskapsfører og økonomisk rådgiver for fondet. Banken forhandler med lånesøkeren om sikkerhet og forøvrig om de nærmere vilkår for lånet, og gir tilråding overfor direktoratet.	3
§ 12. Direktoratet oversender til departementet og Riksrevisjonen kopi av tertialoppgaver som mottas fra Statens nærings- og distriktutviklingsfond, over:	4
§ 13. Søknad om innvilgelse av lån i Statens fond for alkoholfrie overnattings- og serveringssteder treffes av direktoratets styre. Tilråding fra Statens nærings- og distriktutviklingsfond kan innhentes i tvilstilfelle.	4
§ 14. Søknad om avdragsutsettelse, avdragsfrihet, rentefrihet, renteutsettelse og renteakkumulering avgjøres av direktoratet. I tvilstilfelle kan tilråding innhentes fra Statens nærings- og distriktutviklingsfond.	4
§ 15. Søknader om prioritetsvikelse og panteendring avgjøres av direktoratet. Tilråding fra Statens nærings- og distriktutviklingsfond kan innhentes i tvilstilfelle.	4
§ 16. Søknader om avskrivning av uerholdelig fordring på inntil kr 300.000,- avgjøres av direktoratet iht. Rundskriv R-12/92. Søknader om avskrivninger over kr 300.000,- sendes Helse- og omsorgsdepartementet, og avgjøres av Finansdepartementet.	4
§ 17. Vedtak etter § 13 om å avslå lånesøknaden eller innvilge søknad med lavere beløp enn det er søkt om, kan påklages til Helse- og omsorgsdepartementet.	4
§ 18. Forskriften kan endres av Helse- og omsorgsdepartementet.	5
§ 19. Med forskriften oppheves fondsvilkårene fastsatt av Sosialdepartementet i 1951, samt instruksen av 15. mars 1974 (med senere endringer) vedrørende Edruskapsdirektoratets behandling av fondssakene.	5


Hjemmel: Fastsatt ved kgl.res. av 5. juni 1987 med hjemmel i Stortingsvedtak av 19. mai 1951 punkt 5. Fremmet av Sosialdepartementet.
Endringer: Endret ved forskrifter 15 juni 1995 nr. 561, 21 des 2001 nr. 1484, 8 okt 2004 nr. 1395, 10 jan 2006 nr. 62 (bl.a. ansv. dep.), 17 jan 2013 nr. 61.

§ 1. Formålet med fondet er å bidra til å fremme edruskap, særlig blant unge, ved å gi lån til overnattings- og serveringssteder der det ikke serveres alkoholholdige drikkevarer.
Lån gis til kommuner, organisasjoner og andre ved kjøp, bygging, utvidelse eller modernisering av alkoholfrie overnattingssteder og alkoholfrie serveringssteder, herunder fritidssentre for ungdom.

§ 2. Lån til overnattingssteder gis til restaurant- eller kafedrift. Lån til investeringer i forbindelse med overnattingsdriften gis bare unntaksvis når særlige grunner foreligger.

§ 3. I særlige tilfeller kan lån ytes til tegning av nødvendig aksjekapital, eventuelt til kjøp av aksjer i bedrifter som nevnt under § 1.

§ 4. Fondets midler kan ikke nyttes til tilskudd.
Helsedirektoratet kan likevel benytte inntil 50 pst. av årlig renteavkastning til å utvikle prosjekter som kan styrke rusfrie miljøtilbud spesielt for ungdom, i samarbeid med næringens organisasjoner.
Kostnader som gjelder fondets forvaltning belastes fondet.

0
Endret ved forskrifter 15 juni 1995 nr. 561, 21 des 2001 nr. 1484 (i kraft 1 jan 2002), 17 jan 2013 nr. 61.


§ 5. Lån skal sikres ved pant i fast eiendom, – herunder festerett til fast eiendom, – med driftstilbehør. Låntakeren skal holde pantet tilstrekkelig forsikret.
Som sikkerhet for lån kan også godtas pant i leierett til lokaler i fast eiendom med driftstilbehør.
Leieretten/festeretten må være tinglyst og overførbar.

§ 6. Lån skal først utbetales når bygget er ferdig i samsvar med godkjente planer.

§ 7. Avdragene fastsettes etter annuitetsprinsippet med en fast minstesum (renter og avdrag) årlig. Rentesatsen skal være den vanlige for lån fra statens fond. Lånetiden skal være maksimum 20 år.

§ 8. Bedriften må ikke selges eller nyttes til annet formål uten samtykke fra Helsedirektoratet.

0
Endret ved forskrifter 15 juni 1995 nr. 561, 21 des 2001 nr. 1484 (i kraft 1 jan 2002), 17 jan 2013 nr. 61.


§ 9. Bedriftens regnskaper skal revideres av en registrert revisor i henhold til revisorloven. Denne kan gi instruksjoner om regnskapsføringen.

§ 10. Så lenge lånet ikke er tilbakebetalt, skal direktoratet føre slik regnskapsmessig og annen kontroll som det til enhver tid finner nødvendig, og lånetakerne er pliktig til å sende inn alle regnskapsoppgaver m.v. og opplysninger som måtte ønskes til dette formål.

§ 11. Statens nærings- og distriktutviklingsfond er regnskapsfører og økonomisk rådgiver for fondet. Banken forhandler med lånesøkeren om sikkerhet og forøvrig om de nærmere vilkår for lånet, og gir tilråding overfor direktoratet.
Banken forestår innkreving av renter og avdrag. Panteobligasjoner og leiekontrakter som er stilt som sikkerhet for lån av fondet, skal oppbevares i banken.

0
Endret ved forskrift 15 juni 1995 nr. 561.


§ 12. Direktoratet oversender til departementet og Riksrevisjonen kopi av tertialoppgaver som mottas fra Statens nærings- og distriktutviklingsfond, over:
a.
Lån i fondet

b.
Misligholdte lån

c.
Regnskap.


0
Endret ved forskrift 15 juni 1995 nr. 561.


§ 13. Søknad om innvilgelse av lån i Statens fond for alkoholfrie overnattings- og serveringssteder treffes av direktoratets styre. Tilråding fra Statens nærings- og distriktutviklingsfond kan innhentes i tvilstilfelle.
Direktoratet må påse at lånetildelingen holdes innenfor den totalramme for fondsmidler som til en hver tid står til rådighet.

0
Endret ved forskrift 15 juni 1995 nr. 561.


§ 14. Søknad om avdragsutsettelse, avdragsfrihet, rentefrihet, renteutsettelse og renteakkumulering avgjøres av direktoratet. I tvilstilfelle kan tilråding innhentes fra Statens nærings- og distriktutviklingsfond.
Det kan gis avdragsutsettelse for en periode av inntil en termin, men uten at lånets løpetid blir tilsvarende forlenget.
Avdragsfrihet kan gis for to terminer av gangen. Lånets løpetid blir tilsvarende forlenget.
Rentefrihet kan bare gis i forbindelse med at øvrige kreditorer med lik eller dårligere sikkerhet også deltar vanligvis tilsvarende.

0
Endret ved forskrift 15 juni 1995 nr. 561.


§ 15. Søknader om prioritetsvikelse og panteendring avgjøres av direktoratet. Tilråding fra Statens nærings- og distriktutviklingsfond kan innhentes i tvilstilfelle.

0
Endret ved forskrift 15 juni 1995 nr. 561.


§ 16. Søknader om avskrivning av uerholdelig fordring på inntil kr 300.000,- avgjøres av direktoratet iht. Rundskriv R-12/92. Søknader om avskrivninger over kr 300.000,- sendes Helse- og omsorgsdepartementet, og avgjøres av Finansdepartementet.
Ved avsluttet konkursbehandling i aksjeselskap, oppfylt tvangsakkord og frivillig akkord, herunder underhåndsakkord, er beløpsgrensen for avskrivning av udekket beløp kr 500.000,-, jf Rundskriv R-12/92.

0
Endret ved forskrifter 15 juni 1995 nr. 561, 8 okt 2004 nr. 1395, 10 jan 2006 nr. 62.


§ 17. Vedtak etter § 13 om å avslå lånesøknaden eller innvilge søknad med lavere beløp enn det er søkt om, kan påklages til Helse- og omsorgsdepartementet.
Det samme gjelder avgjørelser etter § 14 og etter § 16 for beløp inntil kr 300.000,-. For klager på avskrivninger over kr 300.000,- er Kongen klageinstans. For konkurs og akkordtilfellene er grensene kr 500.000,-.
Klagefristen er 3 uker fra melding om direktoratets avgjørelse er kommet fram til søkeren i rekommandert brev.

0
Endret ved forskrifter 15 juni 1995 nr. 561, 8 okt 2004 nr. 1395, 10 jan 2006 nr. 62.


§ 18. Forskriften kan endres av Helse- og omsorgsdepartementet.

0
Endret ved forskrifter 15 juni 1995 nr. 561, 8 okt 2004 nr. 1395, 10 jan 2006 nr. 62.


§ 19. Med forskriften oppheves fondsvilkårene fastsatt av Sosialdepartementet i 1951, samt instruksen av 15. mars 1974 (med senere endringer) vedrørende Edruskapsdirektoratets behandling av fondssakene.
 
Forskrift om retningslinjer for tildeling av lån fra Statens fond for alkoholfrie overnattings- og serveringssteder.
Side 5
