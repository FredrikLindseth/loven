﻿Utskrift fra Lovdata - 04.04.2019 23:08

 

Forskrift om innsending av kirkebøker til statsarkivene.


Dato
FOR-1953-06-25-2
Departement
Kulturdepartementet
Avd/dir
Kirkeavd.
Publisert
II 1953 s 318
Gjelder for
Norge
Hjemmel
LOV-1814-05-17-§19
Rettet
28.03.2003 (tekstfeil)
Korttittel
Forskrift om kirkebøker til statsarkivene



Innholdsfortegnelse
Forskrift om innsending av kirkebøker til statsarkivene.	1
Innholdsfortegnelse	2


Ved rundskriv av 28. januar 1904 fikk sokneprestene pålegg om å sende inn til sentralarkivene kirkebøker som var utskrevet i 1819, samtlige utskrevne klokkerduplikater og en del gamle arkivsaker ellers. Ved rundskriv av 15. mai 1929 ble det pålagt prestene å sende inn til arkivene kirkebøker utskrevet mellom 1820 og 1840.
Spørsmålet om ny innkalling av kirkebøker ble reist av riksarkivaren i brev av 12. april 1948. Riksarkivaren foreslo at alle kirkebøker som var påbegynt før 1. januar 1876 og utskrevet senest 1880, ble beordret innsendt til statsarkivene. Etter at en del eldre kirkebøker var sterkt skadd ved brann, henstillet videre Statistisk Sentralbyrå i brev av 15. mai 1950 at spørsmålet om å sikre betryggende oppbevaring av de gamle kirkebøker ble tatt opp. Statistisk Sentralbyrå har opplyst at kirkebøker fra før 1866 overhodet ikke lar seg rekonstruere, for årene 1866 til 1902 kan det bare med vanskelighet la seg gjøre, men fra 1903 av er Byråets arkiv fullstendig. Statistisk Sentralbyrå har foreslått at kirkebøker fra 1875 og tidligere år blir innkalt til statsarkivene.
Saken har vært lagt fram for rikets biskoper som har foreslått at det blir truffet bestemmelse om at utskrevne kirkebøker skal sendes inn til statsarkivene når det er gått 90 år siden siste innførsel.
Under hensyn til den store betydning det har at eldre kirkebøker blir betryggende oppbevart og sikret mot brann, finner departementet at utskrevne kirkebøker bør sendes inn til vedkommende statsarkiv eller statsarkivkontor når det er gått 80 år siden siste innførsel. En skal be sokneprestene og andre prester som fører kirkebøker, om snarest mulig å sende inn til vedkommende statsarkiv eller statsarkivkontor de kirkebøker som er utskrevet senest 1872. For fremtiden bes kirkebøker som er utskrevet, sendt inn til statsarkivene uten nærmere melding eller påkrav når det er gått 80 år siden siste innførsel.
Klokkerduplikater som måtte være ført, sendes som hittil inn til vedkommende statsarkiv eller statsarkivkontor så snart de er utskrevet.
Vedkommende statsarkiv eller statsarkivkontor er:
For Østfold, Akershus, Oslo, Buskerud, Vestfold og Telemark fylker statsarkivet i Oslo, for Hedmark og Oppland fylker statsarkivet i Hamar, for Aust-Agder og Vest-Agder fylker statsarkivet i Kristiansand, for Rogaland fylke statsarkivkontoret i Stavanger, for Hordaland, Bergen og Sogn og Fjordane fylker statsarkivet i Bergen, for Møre og Romsdal, Sør-Trøndelag, Nord-Trøndelag og Nordland fylker statsarkivet i Trondheim og for Troms og Finnmark fylker statsarkivkontoret i Tromsø.
Kirkebøkene må ved innsendingen være forsvarlig innpakket. De sendes som verdiforsikret pakke.
Portoutgifter i forbindelse med innsendingen kan dekkes ved tjenestemerker.
Utskrifter og attester fra de kirkebøker som er sendt inn til statsarkivene, blir å levere av statsarkivarene. Begjæringer om utskrifter og attester fra en innsendt kirkebok blir å sende vedkommende statsarkiv eller statsarkivkontor. For så vidt prestene til sin embetsførsel trenger opplysninger fra en innsendt kirkebok, blir disse gitt gratis av vedkommende statsarkiv.
 
Forskrift om innsending av kirkebøker til statsarkivene.
Side 3
