﻿Utskrift fra Lovdata - 04.04.2019 23:10

 

Forskrift om utbetaling til norske sjøfolk som seilte ute i krigsårene.


Dato
FOR-1972-08-25-3
Departement
Nærings- og fiskeridepartementet
Avd/dir
Sjøfartsdir.
Publisert
I 1972 s 1017, II 1972 s. 689
Ikrafttredelse
01.09.1972
Sist endret
FOR-1988-12-23-1081
Gjelder for
Norge
Hjemmel
LOV-1814-05-17-§75
Korttittel
Forskrift om utbetaling til krigsseilere



Innholdsfortegnelse
Forskrift om utbetaling til norske sjøfolk som seilte ute i krigsårene.	1
Innholdsfortegnelse	2
I.	3
II.	3
III.	3
I.	3
II.	4
III.	4
IV.	5
V.	5


Fastsatt ved kgl.res. 25. august 1972.
Endringer: Endret ved vedtak 23. desember 1988 nr. 1081.


I.
I medhold av Stortingets vedtak 27. april 1972 fastsettes forskrifter om nærmere regler for utbetaling til norske sjøfolk som seilte ute i krigsårene i tiden 1. juli 1940 til og med 30. juni 1945 og om gjennomføringen av utbetalingene, i samsvar med et fremlagt utkast.


II.
Handelsdepartementet bemyndiges til å endre forannevnte regler, gjøre fravik fra reglene samt fastsette ytterligere regler i henhold til Stortingets vedtak av 27. april 1972 om utbetalinger til norske sjømenn som seilte ute i krigsårene og om gjennomføring av utbetalingene.


III.
De under I. omhandlede forskrifter settes i kraft fra den tid Handelsdepartementet bestemmer.
Utkastet lyder:


I.

1.
Til norske sjøfolk som seilte for Nortraship i tiden fra 1. juli 1940 til og med 30. juni 1945 utbetales ex gratia kr. 180,- pr. fartsmåned.


1.1
Som norske sjøfolk regnes i denne sammenheng sjømenn som under krigen var norske statsborgere eller som senere er blitt norske statsborgere.


1.2
Som sjømenn regnes personer som var forhyrt til og mønstret på skip i handelsflåten uansett funksjon om bord.


1.3
Som seilas i Nortraship's tjeneste regnes tjenestegjøring utenfor sperresonen på skip disponert av Nortraship eller på skip hvor sjøfolkene var underlagt samme tariffmessige bestemmelser som på skip disponert av Nortraship.


1.4
Som fartstid regnes tjenestegjøring som nevnt under pkt. 1.3. 15 dager eller mer av en kalendermåned regnes som hel fartsmåned, mens 14 dager eller mindre ikke regnes med. Ved avmønstring på grunn av sykdom eller etter forlis, medregnes i fartstiden inntil 2 måneder fra avmønstringen hvis ny forhyring og mønstring ikke har funnet sted innen nevnte tidsrom. Ved avmønstring av annen grunn medregnes inntil 2 måneder av den tid vedkommende mottok poolpenger eller fritidsgodtgjørelse, regnet fra avmønstringen. I fartstiden medregnes den tid som vedkommende sjømann måtte ha mottatt fangenskapslønn for, samt midlertidig tjenestegjøring om bord på allierte skip etter beordring fra norske myndigheter.


2.
Dersom vedkommende sjømann er død og etterlater seg ektefelle, utbetales beløpet til denne.


2.1
Som etterlatt ektefelle regnes den som var gift med vedkommende sjømann på det tidspunkt da denne avgikk ved døden, uansett om nytt ekteskap senere er inngått. Fører utbetalingen til åpenbare urimeligheter f.eks. hvor vedkommende sjømann har vært gift flere ganger og særlig hvor det er barn fra disse ekteskap, kan denne regel fravikes og utbetaling skje etter nærmere retningslinjer som blir å fastsette av Handelsdepartementet.


3.
Er også ektefellen død, utbetales beløpet til avdøde sjømanns barn, eventuelt dennes foreldre dersom vedkommende ikke etterlater seg barn.


3.1
I spørsmål om hvem som er avdøde sjømanns barn eller foreldre, legges til grunn de samme regler som i norsk arverett.


4.
Er det ingen etterlatt ektefelle, barn eller foreldre igjen etter sjømannen, kan utbetaling i helt særegne tilfelle foretas til andre, f.eks. hvor vedkommende sjømann i lengre tid er blitt pleiet av søsken eller andre.


5.
Krav i medhold av Stortingets vedtak av 27. april 1972 er oppstått nevnte dato.



II.

1.
Utbetalingene skal administreres av Direktoratet for sjømenn1 som avgjør hvorvidt fremsatte krav skal imøtekommes.


1.1
Direktoratet for sjømenn1 innhenter de opplysninger som er nødvendig for å avgjøre størrelsen av krav og hvem som er berettiget til utbetaling.


1.2
Direktoratet for sjømenn1 utarbeider skjema som skal fylles ut i forbindelse med krav om utbetaling. Slike skjema blir å legge ut på høvelige offentlige kontorer m.v. etter nærmere bestemmelse av direktoratet.


1.3
Utbetalingen skjer slik Direktoratet for sjømenn1 bestemmer, fortrinnsvis over postgiro.


1.4
I de tilfelle hvor en sjømann har krigspensjon og hvor utbetaling av denne skjer til andre enn den berettigede, skal utbetalinger i henhold til nærværende regler skje etter de samme retningslinjer som for krigspensjonen (forvaltning).


1.5
I den utstrekning det er mulig skal utbetaling til kravshavere født i 1905 eller tidligere gis prioritet foran andre.


1
Sjøfartsdirektoratet, ifølge forskrift 23. desember 1988 nr. 1081.



III.
Mulige klager vedrørende utbetalingen i henhold til Stortingets vedtak av 27. april 1972 og nærværende regler, stiles til Handelsdepartementet og sendes Direktoratet for sjømenn1.

1
Sjøfartsdirektoratet, ifølge forskrift 23. desember 1988 nr. 1081.



IV.
Handelsdepartementet kan endre disse regler, gjøre fravik fra reglene samt fastsette ytterligere regler i henhold til Stortingets vedtak av 27. april 1972 om utbetalinger til norske sjømenn som seilte ute i krigsårene m.v.


V.
Disse forskrifter trer i kraft fra den tid Handelsdepartementet bestemmer.
 
Forskrift om utbetaling til norske sjøfolk som seilte ute i krigsårene.
Side 5
