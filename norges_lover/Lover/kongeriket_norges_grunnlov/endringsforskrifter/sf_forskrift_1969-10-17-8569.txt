﻿Utskrift fra Lovdata - 04.04.2019 23:09

 

Forskrift om fastsettelse av rente på grunnkjøpsobligasjoner


Dato
FOR-1969-10-17-8569
Departement
Finansdepartementet
Avd/dir
Økonomiavd.
Gjelder for
Norge
Hjemmel
Stortingsvedtak, LOV-1814-05-17-§75
Korttittel
Forskrift om rente på grunnkjøpsobl.



Innholdsfortegnelse
Forskrift om fastsettelse av rente på grunnkjøpsobligasjoner	1
Innholdsfortegnelse	2
I	3




I
Rentesatsen for statens grunnkjøpsobligasjoner som utstedes fra og med 17. oktober 1969 til 5 3/4 % p.a.
 
Forskrift om fastsettelse av rente på grunnkjøpsobligasjoner
Side 3
